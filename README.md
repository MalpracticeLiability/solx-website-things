# Your Webstack and YOU!

So you've completed your first website. Congrats! You probably feel like [this](http://cdn1.theodysseyonline.com/files/2014/11/10/6355119118498760371558805978_lazy.gif) after all the work you've been through. The good news is the hard part is now all over! Now comes the easy part: Maintenance. All you need to do is make minor edits periodically and make sure nothing is broken once in a while. This is to help you understand your web stack better to ~~impress interviewers from cool bootcamps~~ make your life easier.

Yes I know I'm cheesy af. You know you like it.


# Stack

Here's how your website is currently functioning:

## Hosting Services

For your website to exist on the website, it needs a domain name, a webhost to make that domain name point to an actual website, and (in your case), an email setup. Here's the details

### Domain Name

Your domain name is simply a custom website name that you own. It does not come with its own website, hosting service, etc. Your domain names, solx2.com and sammamishsolar.com, are currently being hosted through web.com. These should normally cost around $12/yr each to renew. If web.com costs around this price ($10-$13), then it's worth keeping it here to make life easier. If not, contact your favorite long distance IT support guy to help you out. If it turns out renewal is expensive (unlikely), another domain registrar worth considering is namecheap.com. Again, this is unlikely to happen so I wouldn't worry about it too much. 

The only thing you should worry about here is make sure automatic renewal is turned on. That way when the domain is about to expires, it automatically charges a credit card to extend it another year. 

### Hosting

Hosting is independent of domain name. The hosting is the actual server setup that contains your site files and gives it to browsers that want it. Your hosting is currently being done through web.com. This is not ideal, as web.com is expensive. You do not need to be paying $1200/yr to host a website nor do you need to bundle maintenance with this. Again, maintenance is easy. Its literally just making sure things aren't unexpectedly broken.

For this website, you'll be moving hosting to heroku.com. More on this later. The main advantages of heroku.com are a proper, flexible backend setup, free hosting for a website, and deploying to Heroku looks great on a resume.

### Email

Email has to be tied to a domain name. That means if you change domain name registrars, you also change business email providers. Obviously this is not ideal. Luckily though, like I mentioned under Domain Name, web.com's domain registration fees should be the same as everyone else (+/- $1). Therefore I would heavily suggest keeping both email and domain name with web.com to make life easier for the client.

### Summary

So to summarize what you should do on your end:

* Domain Name (sammamishsolar.com, solx2.com) and web email (info@solx2.com) should be kept on web.com. 
* Hosting and maintenance should be **cancelled** on web.com and moved to heroku.com

## Front End

Now here's your favorite part, the front end! I've made some minor changes to it but it should be straightforward to you

### HTML

Obviously, the markup language of the site is done in HTML. You have your home page @ `index.html`, along with seven other `.html` files. These files are found in `/public`. More on this later when we get to the backend.

### CSS

The design of the page is done in pure "vanilla" CSS. There are no frameworks here. Your main CSS file, `main.css`, is located in `/public`. You have a secondary CSS file, named `slideshow.css`, for your slideshow on `about.html` located in `/public/css`. I don't know why, it was like that when I got the files so I just preserved it.

### JavaScript

This is what gives the site functionality. On your part you have some JavaScript scripts in your HTML. You have one on `contact.html` for Google Maps. You have another one on `recent-projects.html` for your slideshow.

I also added some additional front-end JavaScript for form functionality. On both `index.html` and `contact.html`, I added JQuery for AJAX. JQuery is a custom JavaScript functionality for the front end. There is a `jquery-3.3.1.min.js` file for JQuery located in `/public`, and there is an import statement on Line 10 on both `index.html` and `contact.html` to be able to use the library.

The reason I did this is because JQuery includes AJAX. AJAX, short for "Asynchronous JavaScript And XML", is functionality that allows the browser to listen for a server response. For your website, I did this so the browser can figure out if the contact email succeeded or failed, and the browser would give an appropriate popup to the client. If the email went through, the popup says, "Thank you for your interest!\nWe will respond to you soon". If it fails, the popup responds, ""Sorry, your email did not go through.\nPlease try again or contact us directly at \'info@solx2.com\'".

The JQuery script can be found on Line 176 of `index.html` and Line 161 of `contact.html`. The script works like this:

1. `$(document).ready(function() {` loads the script *after* the page is finished loading. If the script starts loading before, it will break because the contact form isn't rendered yet.
2. `$("#contact-form").submit(function(e){` ties the script to the contact form (specifically, an id of `contact-form`). 
3. When a user clicks submit, it sets off what's called an "event". The line above listens for this event. When it detects the submit button event, the rest of the script fires.
4. `e.preventDefault();` stops the default action of the form submit. We don't want the form to handle submission things - we want our script to do it.
5. `type: "post",` specifies that we want our form to send data as an HTTP:POST request. There's two basic HTTP requests: GET and POST. There's more nuance to this, but in a nutshell: GET is for receiving data from a server, POST is for sending data to a server. 
6. `url: "/contact-form",` sends our HTTP request to this URL. Note that this URL defined by how you write your server. I purposely set the server to listen to this specific URL, and when it detects an HTTP request, it *knows* that it came from a contact form submission.
7. Don't worry about the next two lines in the script
8. `success: function(response){` fires when the email is successfully sent. The server is configured to send a success message (specifically, HTTP 200, more on this later) back to the client if the email is successful.
9. `error: function(response){` fires when the email fails to send. The server is configured to send an error message (specifically, HTTP 400) back to the client if the email fails to send.

And that's it! 
		            
## Backend

Here's where it'll get complicated. Feel free to ask if you have any questions.

### Heroku

Your site is hosted on heroku.com. Heroku is a *platform as a service*, or *PaaS*. Basically, it gives you the entire infrastructure for running custom apps without us having to worry about it ourselves. I don't have to worry about a file server, a load balancer, a DNS, scaling performance, etc. All I see and care about is I can push code and Heroku runs the code for me.

I chose Heroku because it gives me immense flexibility while allowing for effectively free web hosting. With Heroku, I can deploy Ruby on Rails or node.js web apps and backend code. Most traditional web hosts don't let you use either, let alone both.

Heroku has multiple tiers. We're using the *free* tier. Free gives you 1000 "dyno hours" per month (550 dyno hours initially, bumped up to 1000 when you add a credit/debit card). A dyno hour is an actual hour of server runtime. If your website runs for 50 hours a month, then you use 50 dyno hours per month. That's it. There is one major limitation: Each free app (including websites) goes to sleep if no one uses it for 30 minutes. This means if no one uses your website for 30 minutes, it goes to sleep and the next time someone accesses it, it'll take a while to load because the dyno has to spin back up. I got around this by deploying a *second* app that pings the website every 10 minutes. To do this, you just have to write a simple node.js file with the following lines:
	`var http = require("http");`
`http.get("http://solxsitetest.herokuapp.com/");`

Then you upload it to Heroku. You then go into your app settings on heroku.com, install Scheduler, and set it to run `$ node <app-name>.js` every 10 minutes.

You have a budget of 1000 dyno hours per month to work with. There are ~730 hours in a month, and pinging the website should add another ~12 hours a month. Therefore you're 250 hours *below* the limit so you are going to get free web hosting. If you're still nervous, Heroku sends out an alert when you reach 80% of your hours a month, or 800 hours.

Setting up and deploying a Heroku app will take about 30 mins and some commands I'll tell you. Maintaining it though is easy. All you need to do is periodically check to see if your site is broken. If it is, just open Terminal, `$ cd` into your project directory, and run `$ heroku restart <app-name>`. To update code on Heroku, you just run three commands:
`$ git add .`
`$ git commit -m "<YOUR-COMMIT-MESSAGE-HERE>`
`$ git push heroku master`

And that's it!

### node.js
node.js is a custom JavaScript runtime that allows for JavaScript to be run in any environment. "Vanilla" JavaScript is normally only able to run in browser. For example, you can't actually write a JavaScript program on your computer and run it the way you would a Ruby program. To do that, you'd need a JavaScript runtime that can run JS code outside a browser. Enter node.js. Similarly, node.js lets a server run JavaScript - an environment that vanilla JS is not allowed to be run on.

In addition to node.js, you have multiple node.js libraries (called **modules**) that are used:

* Express.js: Express is a framework that, among other things, allows for very quick writing of web apps and server code. I used it because it handles a lot of the server things "under the hood", allowing me to do more while writing less lines. 
* nodemailer: nodemailer is a node.js library that allows you to easily send emails. It can be configured to any email server (provided it's actually running... looking at you web.com). 
* jquery: For the front-end as noted above. You still need the actual files, and for simplicity's sake I listed this as a dependency.

### Server Files

There are a few files on the backend you should be aware of. These are very important and need to be written properly for everything to run.

* **Procfile**: This file (short for Process file) is the file that Heroku looks at when starting up your web app. It tells Heroku exactly what to run and how to run it. Procfile should be named exactly like that, with no file extension. No .txt or .md or .js. Our Procfile contains exactly one line: `web: node app.js`. This tells Heroku to spin up a web dyno (since we're running a web app), then run the command `$ node app.js`. `$ node` is the command used to run a node.js file, and `app.js` is the file that contains our server code. So in short, our Procfile tells Heroku to run our server file when it starts up.
* **app.js**: This is our file containing all our code for our server. There's a lot on here but I'll cover the basics:
	* Lines 1-5 tells Heroku what modules we're using. This way, when Heroku comes across a function from one of those modules, it knows where to look for it.
	* Lines 8-10 tells our server where our files are. Notice how all our files are stored in `/public`.  Line 9 tells our server that any file request it gets to add `/public` to the beginning of the file directory. For example, if someone clicks on "About", the browser asks for `about.html`. The server sees this and automatically looks for the file at `/public/about.html`, where our file is actually located. Line 10 does something similar for photos.
	* Lines 14-16 tells the server what our default webpage is. So when someone access our server for the first time, it's actually accessing `/`, or "root". This line tells the server that when someone connects to our server, to give them `index.html`.
	* Lines 18-55 handles the email logic. 
		* On Line 18, it listens for a HTTP POST request to `/contact-form`. See earlier on the front end to see how we tied our form into here with our AJAX script.
		* Lines 25-26 tells how to connect to the smtp server. Note: smtp is the name of the protocol that *sends out* emails. It's worth noting that SMTP always happens on Port 587 if it's unencrypted/not using SSL, Port 465 if it's encrypted/using SSL, or Port 25 if the smtp server is old and outdated.
		* Lines 29-30 is our login credentials. Note that this is using Heroku's config vars for security
		* Lines 33-41 is where we format our email using the data from the form
		* Line 42-47 handles our errors if our email fails to send. With the way it's set up, it logs the error and sends an HTTP 400 response, then redirects the user to the previous page. [HTTP 400](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/400) is the HTTP code for "Bad Request". As for redirecting the user to the previous page, this sends them away from `/contact-form`, so they essentially "stay" on the same webpage after pressing Submit.
		* Lines 48-52 function similarly. It sends an [HTTP 200](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/200) response, code for "OK", then redirects the user back to the page they came from.
		* Line 59 tells the server to listen continuously for communication on Port 3000. Why 3000? I have no clue. It's just the default.
* **package.json**: This is where we list all our dependencies for our node.js app. Every time you run `$ npm install <module-name>`, e.g. `$ npm install express`, it'll automatically save the module name to `package.json`. Conversely, if you list a module here without installing it first, then running `$npm install` will install all those dependencies. This means you can copy/paste this `package.json` file into your folder, then you can run `$ npm install` to download + install express, nodemailer, and jquery.

# Final Words

You're almost at the finish line! I know it's been stressful and at times [overwhelming](https://i.imgur.com/BKnW7BA.gif) for you, but you got this. You've made an unbelievable amount of progress in such a short amount of time. Once all this is done, I think you totally deserve to [treat yourself](https://i.imgur.com/4cWQ9CY.gif).

I spent way more time than I'm willing to admit to looking for the right GG gifs. Worth it.


https://bitbucket.org/MalpracticeLiability/solx-website-things/src